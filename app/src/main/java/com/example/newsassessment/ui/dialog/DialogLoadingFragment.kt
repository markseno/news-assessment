package com.example.newsassessment.ui.dialog

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.newsassessment.base.BaseDialog
import com.example.newsassessment.databinding.DialogLoadingBinding

class DialogLoadingFragment : BaseDialog<DialogLoadingBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> DialogLoadingBinding
        get() = DialogLoadingBinding::inflate

    override fun bindView() {
        isCancelable = false
    }

    override fun assignListener() {
    }
}




