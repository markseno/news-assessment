package com.example.newsassessment.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.example.newsassessment.data.NewsRepository
import com.example.newsassessment.data.models.News
import javax.inject.Inject
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Named

class MainViewModel @Inject constructor(
    private val repository: NewsRepository,
    @Named("IO") private val io: CoroutineDispatcher
) : ViewModel() {

    fun fetchTopHeadlinesNews(): LiveData<PagingData<News>> {
        val topHeadlinesNews = MutableLiveData<PagingData<News>>()
        viewModelScope.launch {
            repository.getTopHeadlinesNews()
                .flowOn(io)
                .collectLatest {
                    topHeadlinesNews.value = it
                }
        }
        return topHeadlinesNews
    }

}