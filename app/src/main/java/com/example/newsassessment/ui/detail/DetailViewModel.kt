package com.example.newsassessment.ui.detail

import androidx.lifecycle.ViewModel
import com.example.newsassessment.data.NewsRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Named

class DetailViewModel @Inject constructor(
    private val repository: NewsRepository,
    @Named("IO") private val io: CoroutineDispatcher
) : ViewModel()