package com.example.newsassessment.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsassessment.R
import com.example.newsassessment.data.models.News
import com.example.newsassessment.databinding.ItemNewsBinding

class MainAdapter(private val clicked: (News) -> Unit) :
    PagingDataAdapter<News, MainAdapter.NewsViewHolder>(
        MoviesDiffCallback()
    ) {

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {

        return NewsViewHolder(
            ItemNewsBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    inner class NewsViewHolder(private var binding: ItemNewsBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: News?) = binding.apply {
            root.setOnClickListener {
                if (item != null) {
                    clicked.invoke(item)
                }
            }

            Glide.with(root.context)
                .load(item?.urlToImage)
                .error(ContextCompat.getDrawable(itemView.context, R.color.greyLight))
                .into(ivNews)

            val titleNews = item?.title ?: "-"
            val author = item?.author ?: "-"
            tvTitleNews.text = if (titleNews.length > 70) titleNews.substring(0,70).plus("...") else titleNews
            tvAuthor.text = if (author.length > 45) author.substring(0,45).plus("...") else author

        }
    }

    private class MoviesDiffCallback : DiffUtil.ItemCallback<News>() {
        override fun areItemsTheSame(oldItem: News, newItem: News): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: News, newItem: News): Boolean {
            return oldItem == newItem
        }
    }
}