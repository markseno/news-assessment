package com.example.newsassessment.ui.main

import android.content.Intent
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsassessment.R
import com.example.newsassessment.databinding.ActivityMainBinding
import com.example.newsassessment.base.BaseActivity
import com.example.newsassessment.data.models.News
import com.example.newsassessment.di.injectViewModel
import com.example.newsassessment.ui.detail.DetailActivity
import com.example.newsassessment.ui.dialog.DialogErrorFragment
import com.example.newsassessment.utils.Cons.ERROR_DIALOG
import com.example.newsassessment.utils.Cons.LOADING_DIALOG
import com.example.newsassessment.utils.Cons.NEWS_DATA

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() ,
    DialogErrorFragment.DialogErrorFragmentListener{

    private lateinit var mainAdapter: MainAdapter

    override fun injectViewModel() { mViewModel = injectViewModel(viewModelFactory) }

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java

    override fun getLayoutResourceId(): Int = R.layout.activity_main

    override fun getViewBinding(): ActivityMainBinding { return ActivityMainBinding.inflate(layoutInflater) }

    override fun initView() {
        setupAdapter()
    }

    override fun initListener() {
    }

    private fun setupAdapter() {
        mainAdapter = MainAdapter { gotoDetailNews(it) }

        binding.rvNews.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
            adapter = mainAdapter.withLoadStateFooter(
                footer = MainLoadingStateAdapter { mainAdapter.retry() }
            )
        }

        mainAdapter.addLoadStateListener { loadState ->
            if (loadState.refresh is LoadState.Loading ||
                loadState.append is LoadState.Loading)
                if (mainAdapter.itemCount == 0) showLoadingDialog()
            else {
                dismissDialog(LOADING_DIALOG)
                val errorState = when {
                    loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                    loadState.prepend is LoadState.Error ->  loadState.prepend as LoadState.Error
                    loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                    else -> null
                }
                errorState?.let {
                    if (mainAdapter.itemCount == 0) showErrorDialog(it.error.message?:"Unknown Error")
                }
            }
        }

        fetchTopHeadlinesNews()
    }

    private fun fetchTopHeadlinesNews() {
        viewModel.fetchTopHeadlinesNews().observe(this) {
            mainAdapter.submitData(this@MainActivity.lifecycle, it)
        }
    }

    private fun gotoDetailNews(news: News) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(NEWS_DATA, news)
        startActivity(intent)
    }

    override fun onDialogErrorFragmentListener() {
        dismissDialog(ERROR_DIALOG)
        fetchTopHeadlinesNews()
    }
}