package com.example.newsassessment.ui.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.Html
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.example.newsassessment.R
import com.example.newsassessment.base.BaseActivity
import com.example.newsassessment.data.models.News
import com.example.newsassessment.databinding.ActivityDetailBinding
import com.example.newsassessment.di.injectViewModel
import com.example.newsassessment.utils.Cons.NEWS_DATA
import com.example.newsassessment.utils.convertDateFormat


class DetailActivity : BaseActivity<ActivityDetailBinding, DetailViewModel>(){

    private var newsData: News? = null

    override fun injectViewModel() { mViewModel = injectViewModel(viewModelFactory) }

    override fun getViewModelClass(): Class<DetailViewModel> = DetailViewModel::class.java

    override fun getLayoutResourceId(): Int = R.layout.activity_main

    override fun getViewBinding(): ActivityDetailBinding { return ActivityDetailBinding.inflate(layoutInflater) }

    override fun initView() {
        newsData = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra(NEWS_DATA, News::class.java)
        } else {
            intent.getParcelableExtra(NEWS_DATA)
        }

        spreadDataIntent(newsData)
    }

    override fun initListener() {
        binding.ivBack.setOnClickListener {
            finish()
        }

        binding.tvContent.setOnClickListener {
            openChrome(newsData?.url ?:"")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun spreadDataIntent(newsData: News?) {
        binding.apply {
            Glide.with(this@DetailActivity)
                .load(newsData?.urlToImage)
                .error(ContextCompat.getDrawable(this@DetailActivity, R.color.greyLight))
                .into(ivImage)

            tvTitleNews.text = newsData?.title ?:"-"
            tvAuthor.text = newsData?.author ?:"-"
            val contentPre = newsData?.content?.substringBefore("[+") ?:"-"
            val content = "<font color=\"#232222\">$contentPre</font><font color=\"#0000FF\">Read More</font>"
            tvContent.text = Html.fromHtml(content)
            tvPublishedAt.text = "Published At " + convertDateFormat(newsData?.publishedAt?:"-")
        }
    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun openChrome(url: String) {
        val uri = Uri.parse("googlechrome://navigate?url=$url")
        val i = Intent(Intent.ACTION_VIEW, uri)
        if (i.resolveActivity(packageManager) == null) {
            i.data = Uri.parse(url)
        }
        startActivity(i)
    }
}