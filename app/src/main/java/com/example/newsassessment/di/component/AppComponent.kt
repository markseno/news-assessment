package com.example.newsassessment.di.component

import com.example.newsassessment.NewsAssessmentApp
import com.example.newsassessment.di.module.ActivityBuilder
import com.example.newsassessment.di.module.AppModule
import com.example.newsassessment.di.module.NetworkModule
import com.example.newsassessment.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ActivityBuilder::class,
        AppModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        AndroidSupportInjectionModule::class
    ]
)

interface AppComponent : AndroidInjector<NewsAssessmentApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: NewsAssessmentApp): Builder

        fun build(): AppComponent
    }

}