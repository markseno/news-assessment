package com.example.newsassessment.di.module

import android.app.Application
import android.content.Context
import com.example.newsassessment.NewsAssessmentApp
import com.example.newsassessment.data.remote.NewsService
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: NewsAssessmentApp): Context = app

    @Provides
    @Singleton
    fun provideApplication(app: NewsAssessmentApp): Application = app

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): NewsService =
        retrofit.create(NewsService::class.java)

    @Provides
    @Singleton
    @Named("IO")
    fun provideBackgroundDispatchers(): CoroutineDispatcher =
        Dispatchers.IO

    @Provides
    @Singleton
    @Named("MAIN")
    fun provideMainDispatchers(): CoroutineDispatcher =
        Dispatchers.Main
}