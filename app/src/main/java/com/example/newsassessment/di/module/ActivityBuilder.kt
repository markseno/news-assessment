package com.example.newsassessment.di.module

import com.example.newsassessment.ui.detail.DetailActivity
import com.example.newsassessment.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributesDetailActivity(): DetailActivity
}