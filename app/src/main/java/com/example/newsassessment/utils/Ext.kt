package com.example.newsassessment.utils

import android.app.Activity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import java.text.SimpleDateFormat
import java.util.Locale

fun Activity?.showDialogExt(dialogFragment: DialogFragment, tag: String?, fragmentManager: FragmentManager?) {
    if (!dialogFragment.isAdded) {
        this?.let {
            if (!it.isFinishing && !it.isDestroyed) {
                try {
                    fragmentManager?.let { it1 -> dialogFragment.show(it1, tag) }
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                }
            }
        }
    }
}

fun dismissDialogExt(tag: String, fragmentManager: FragmentManager?) {
    fragmentManager?.fragments?.forEach { fragment ->
        if (fragment is DialogFragment && fragment.tag == tag) {
            fragment.dismiss()
        }
    }
}


fun isDialogFragmentShowing(tag: String?, fragmentManager: FragmentManager?): Boolean {
    fragmentManager?.fragments?.forEach { fragment ->
        if (fragment is DialogFragment && fragment.tag == tag) {
            return fragment.dialog?.isShowing == true
        }
    }
    return false
}

fun convertDateFormat(inputDate: String): String {
    return try {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val date = inputFormat.parse(inputDate)
        val outputFormat = SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH)
        outputFormat.format(date!!)
    } catch (e: Exception) {
        e.printStackTrace()
        "-"
    }
}


