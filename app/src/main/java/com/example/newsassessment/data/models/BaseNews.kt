package com.example.newsassessment.data.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BaseNews(
    val status: String,
    val totalResults: Int,
    val articles: List<News>
) : Parcelable


