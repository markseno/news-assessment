package com.example.newsassessment.data.remote

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.newsassessment.BuildConfig.NEWS_API_KEY
import com.example.newsassessment.data.models.News
import com.example.newsassessment.data.remote.NewsService.Companion.STARTING_PAGE_INDEX
import retrofit2.HttpException
import java.io.IOException

class NewsPagingRemoteDataSource(
    private val service: NewsService
): PagingSource<Int, News>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, News> {
        return try {
            val page = params.key ?: STARTING_PAGE_INDEX
            val response = service.getTopHeadlinesNews(
                NEWS_API_KEY,
                page,
                "us"
            )
            val news = response.body()?.articles?: emptyList()
            LoadResult.Page(
                data = news,
                prevKey = if (page == STARTING_PAGE_INDEX) null else page.minus(1),
                nextKey = if (page >= calculateTotalPages(response.body()?.totalResults?: 0)) null else page.plus(1)
            )
        } catch (exception: IOException) {
            val error = IOException("Please Check Internet Connection")
            LoadResult.Error(error)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }

    }

    override fun getRefreshKey(state: PagingState<Int, News>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    private fun calculateTotalPages(totalResults: Int): Int {
        val totalPages = totalResults / 20
        return if (totalResults % 20 != 0) {
            totalPages + 1
        } else {
            totalPages
        }
    }

}