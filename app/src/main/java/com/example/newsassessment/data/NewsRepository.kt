package com.example.newsassessment.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.newsassessment.data.models.News
import com.example.newsassessment.data.remote.NewsPagingRemoteDataSource
import com.example.newsassessment.data.remote.NewsService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NewsRepository @Inject constructor(
    private val service: NewsService
) {

    fun getTopHeadlinesNews(): Flow<PagingData<News>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = 20),
            pagingSourceFactory = {
                NewsPagingRemoteDataSource(service)
            }
        ).flow
    }

}