package com.example.newsassessment.data.remote

import com.example.newsassessment.data.models.BaseNews
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsService {

    companion object {
        const val BASE_URL = "https://newsapi.org/v2/"
        const val STARTING_PAGE_INDEX = 1
    }

    @GET("top-headlines")
    suspend fun getTopHeadlinesNews(
        @Query("apiKey") api_key: String?,
        @Query("page") page: Int?,
        @Query("country") country: String?
    ):  Response<BaseNews>
}